macro_rules! impl_hmac {
    ($($hash:ty)*) => {
        $(
            impl $crate::imports::Factory for $hash {
                #[cfg(feature = "hmac")]
                fn build_mac_digest(key: &[u8]) -> Box<dyn $crate::imports::NamedDynDigest> {
                    $crate::imports::Box::new(
                        <$crate::imports::Hmac::<Self> as $crate::imports::KeyInit>::new_from_slice(key).unwrap()
                    )
                }
            }
        )*
    }
}

macro_rules! impl_simple_hmac {
    ($($hash:ty)*) => {
        $(
            impl $crate::imports::Factory for $hash {
                #[cfg(feature = "hmac")]
                fn build_mac_digest(key: &[u8]) -> Box<dyn $crate::imports::NamedDynDigest> {
                    $crate::imports::Box::new(
                        <$crate::imports::SimpleHmac::<Self> as $crate::imports::KeyInit>::new_from_slice(key).unwrap()
                    )
                }
            }
        )*
    }
}

#[cfg(feature = "blake2")]
impl_simple_hmac!(blake2::Blake2b512 blake2::Blake2s256);
#[cfg(feature = "blake3")]
impl_simple_hmac!(blake3::Hasher);
#[cfg(feature = "fsb")]
impl_hmac!(fsb::Fsb160 fsb::Fsb224 fsb::Fsb256 fsb::Fsb384 fsb::Fsb512);
#[cfg(feature = "gost94")]
impl_hmac!(gost94::Gost94CryptoPro gost94::Gost94s2015);
#[cfg(feature = "groestl")]
impl_hmac!(groestl::Groestl224 groestl::Groestl256 groestl::Groestl384 groestl::Groestl512);
#[cfg(feature = "md2")]
impl_hmac!(md2::Md2);
#[cfg(feature = "md4")]
impl_hmac!(md4::Md4);
#[cfg(feature = "md5")]
impl_hmac!(md5::Md5);
#[cfg(feature = "ripemd")]
impl_hmac!(ripemd::Ripemd160 ripemd::Ripemd256 ripemd::Ripemd320);
#[cfg(feature = "sha1")]
impl_hmac!(sha1::Sha1);
#[cfg(feature = "sha2")]
impl_hmac!(sha2::Sha224 sha2::Sha256 sha2::Sha384 sha2::Sha512 sha2::Sha512_224 sha2::Sha512_256);
#[cfg(feature = "sha3")]
impl_hmac!(sha3::Sha3_224 sha3::Sha3_256 sha3::Sha3_384 sha3::Sha3_512);
#[cfg(feature = "shabal")]
impl_hmac!(shabal::Shabal192 shabal::Shabal224 shabal::Shabal256 shabal::Shabal384 shabal::Shabal512);
#[cfg(feature = "sm3")]
impl_hmac!(sm3::Sm3);
#[cfg(feature = "streebog")]
impl_hmac!(streebog::Streebog256 streebog::Streebog512);
#[cfg(feature = "tiger")]
impl_hmac!(tiger::Tiger tiger::Tiger2);
#[cfg(feature = "whirlpool")]
impl_hmac!(whirlpool::Whirlpool);

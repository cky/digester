use std::fmt;
use std::marker::PhantomData;

use crypto_common::typenum::{IsLess, Le, NonZero, U256};
use crypto_common::AlgorithmName;
#[cfg(feature = "hmac")]
use crypto_common::BlockSizeUser;
use digest::core_api::{BufferKindUser, CoreWrapper};
#[cfg(feature = "hmac")]
use digest::Digest;
#[cfg(feature = "hmac")]
use hmac::SimpleHmac;

use super::traits::StaticAlgorithmName;

impl<A: AlgorithmName + BufferKindUser> StaticAlgorithmName for CoreWrapper<A>
where
    A::BlockSize: IsLess<U256>,
    Le<A::BlockSize, U256>: NonZero,
{
    fn algorithm_name() -> String {
        struct Helper<A>(PhantomData<A>);
        impl<A: AlgorithmName> fmt::Display for Helper<A> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                A::write_alg_name(f)
            }
        }

        Helper(PhantomData::<A>).to_string()
    }
}

#[cfg(feature = "hmac")]
impl<A: StaticAlgorithmName + Digest + BlockSizeUser> StaticAlgorithmName for SimpleHmac<A> {
    fn algorithm_name() -> String {
        format!("Hmac<{}>", A::algorithm_name())
    }
}

#[cfg(feature = "blake3")]
impl StaticAlgorithmName for blake3::Hasher {
    fn algorithm_name() -> String {
        "Blake3".to_owned()
    }
}

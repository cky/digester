pub use std::boxed::Box;

#[cfg(feature = "hmac")]
pub use digest::KeyInit;
#[cfg(feature = "hmac")]
pub use hmac::{Hmac, SimpleHmac};

pub use super::traits::{Factory, NamedDynDigest};

use std::ffi::OsString;
use std::fs::File;
#[cfg(feature = "hmac")]
use std::io::Read;
use std::io::{stdin, BufRead, BufReader, Error as IoError, Result as IoResult};
#[cfg(feature = "hmac")]
use std::os::unix::ffi::OsStringExt;
#[cfg(feature = "hmac")]
use std::os::unix::io::{FromRawFd, RawFd};
use std::path::Path;
use std::process;

use clap::{ArgEnum, Parser};
use digest::DynDigest;

use digester::traits::{Factory, NamedDynDigest};

#[derive(Parser)]
#[clap(version, author, about)]
struct Options {
    /// Hash algorithm to use
    #[clap(short, long, arg_enum)]
    algorithm: Algorithm,
    /// Reverse the format of the output
    #[clap(short, long)]
    reverse: bool,
    /// MAC key
    #[cfg(feature = "hmac")]
    #[clap(long, env, hide_env_values(true), parse(from_os_str))]
    mac_key: Option<OsString>,
    /// File descriptor for MAC key
    #[cfg(feature = "hmac")]
    #[clap(long)]
    mac_key_fd: Option<RawFd>,
    /// Files to hash; stdin by default
    #[clap(parse(from_os_str))]
    files: Vec<OsString>,
}

struct ProcessedOptions {
    algorithm: Algorithm,
    reverse: bool,
    #[cfg(feature = "hmac")]
    mac_key: Option<Vec<u8>>,
    files: Vec<OsString>,
}

impl TryFrom<Options> for ProcessedOptions {
    type Error = IoError;

    fn try_from(options: Options) -> IoResult<Self> {
        #[cfg(feature = "hmac")]
        let mac_key = if let Some(fd) = options.mac_key_fd {
            if options.mac_key.is_some() {
                eprintln!("--mac-key-fd specified, ignoring $MAC_KEY");
            }
            let mut result = Vec::new();
            unsafe { File::from_raw_fd(fd) }.read_to_end(&mut result)?;
            Some(result)
        } else {
            options.mac_key.map(OsStringExt::into_vec)
        };
        Ok(Self {
            algorithm: options.algorithm,
            reverse: options.reverse,
            #[cfg(feature = "hmac")]
            mac_key,
            files: options.files,
        })
    }
}

impl ProcessedOptions {
    fn new_hasher(&self) -> Box<dyn NamedDynDigest> {
        #[cfg(feature = "hmac")]
        if let Some(ref key) = self.mac_key {
            return self.algorithm.new_mac_instance(key);
        }
        self.algorithm.new_instance()
    }
}

#[derive(ArgEnum, Clone, Copy, Debug, Eq, PartialEq)]
enum Algorithm {
    #[cfg(feature = "blake2")]
    Blake2b,
    #[cfg(feature = "blake2")]
    Blake2s,
    #[cfg(feature = "blake3")]
    Blake3,
    #[cfg(feature = "fsb")]
    Fsb160,
    #[cfg(feature = "fsb")]
    Fsb224,
    #[cfg(feature = "fsb")]
    Fsb256,
    #[cfg(feature = "fsb")]
    Fsb384,
    #[cfg(feature = "fsb")]
    Fsb512,
    #[cfg(feature = "gost94")]
    Gost94CryptoPro,
    #[cfg(feature = "gost94")]
    Gost94s2015,
    #[cfg(feature = "groestl")]
    #[clap(name = "grøstl224")]
    Grøstl224,
    #[cfg(feature = "groestl")]
    #[clap(name = "grøstl256")]
    Grøstl256,
    #[cfg(feature = "groestl")]
    #[clap(name = "grøstl384")]
    Grøstl384,
    #[cfg(feature = "groestl")]
    #[clap(name = "grøstl512")]
    Grøstl512,
    #[cfg(feature = "md2")]
    Md2,
    #[cfg(feature = "md4")]
    Md4,
    #[cfg(feature = "md5")]
    Md5,
    #[cfg(feature = "ripemd")]
    Ripemd160,
    #[cfg(feature = "ripemd")]
    Ripemd256,
    #[cfg(feature = "ripemd")]
    Ripemd320,
    #[cfg(feature = "sha1")]
    Sha1,
    #[cfg(feature = "sha2")]
    Sha224,
    #[cfg(feature = "sha2")]
    Sha256,
    #[cfg(feature = "sha2")]
    Sha384,
    #[cfg(feature = "sha2")]
    Sha512,
    #[cfg(feature = "sha2")]
    Sha512_224,
    #[cfg(feature = "sha2")]
    Sha512_256,
    #[cfg(feature = "sha3")]
    Sha3_224,
    #[cfg(feature = "sha3")]
    Sha3_256,
    #[cfg(feature = "sha3")]
    Sha3_384,
    #[cfg(feature = "sha3")]
    Sha3_512,
    #[cfg(feature = "shabal")]
    Shabal192,
    #[cfg(feature = "shabal")]
    Shabal224,
    #[cfg(feature = "shabal")]
    Shabal256,
    #[cfg(feature = "shabal")]
    Shabal384,
    #[cfg(feature = "shabal")]
    Shabal512,
    #[cfg(feature = "sm3")]
    Sm3,
    #[cfg(feature = "streebog")]
    Streebog256,
    #[cfg(feature = "streebog")]
    Streebog512,
    #[cfg(feature = "tiger")]
    Tiger,
    #[cfg(feature = "tiger")]
    Tiger2,
    #[cfg(feature = "whirlpool")]
    Whirlpool,
}

impl Algorithm {
    fn new_instance(&self) -> Box<dyn NamedDynDigest> {
        match *self {
            #[cfg(feature = "blake2")]
            Self::Blake2b => blake2::Blake2b512::build_digest(),
            #[cfg(feature = "blake2")]
            Self::Blake2s => blake2::Blake2s256::build_digest(),
            #[cfg(feature = "blake3")]
            Self::Blake3 => blake3::Hasher::build_digest(),
            #[cfg(feature = "fsb")]
            Self::Fsb160 => fsb::Fsb160::build_digest(),
            #[cfg(feature = "fsb")]
            Self::Fsb224 => fsb::Fsb224::build_digest(),
            #[cfg(feature = "fsb")]
            Self::Fsb256 => fsb::Fsb256::build_digest(),
            #[cfg(feature = "fsb")]
            Self::Fsb384 => fsb::Fsb384::build_digest(),
            #[cfg(feature = "fsb")]
            Self::Fsb512 => fsb::Fsb512::build_digest(),
            #[cfg(feature = "gost94")]
            Self::Gost94CryptoPro => gost94::Gost94CryptoPro::build_digest(),
            #[cfg(feature = "gost94")]
            Self::Gost94s2015 => gost94::Gost94s2015::build_digest(),
            #[cfg(feature = "groestl")]
            Self::Grøstl224 => groestl::Groestl224::build_digest(),
            #[cfg(feature = "groestl")]
            Self::Grøstl256 => groestl::Groestl256::build_digest(),
            #[cfg(feature = "groestl")]
            Self::Grøstl384 => groestl::Groestl384::build_digest(),
            #[cfg(feature = "groestl")]
            Self::Grøstl512 => groestl::Groestl512::build_digest(),
            #[cfg(feature = "md2")]
            Self::Md2 => md2::Md2::build_digest(),
            #[cfg(feature = "md4")]
            Self::Md4 => md4::Md4::build_digest(),
            #[cfg(feature = "md5")]
            Self::Md5 => md5::Md5::build_digest(),
            #[cfg(feature = "ripemd")]
            Self::Ripemd160 => ripemd::Ripemd160::build_digest(),
            #[cfg(feature = "ripemd")]
            Self::Ripemd256 => ripemd::Ripemd256::build_digest(),
            #[cfg(feature = "ripemd")]
            Self::Ripemd320 => ripemd::Ripemd320::build_digest(),
            #[cfg(feature = "sha1")]
            Self::Sha1 => sha1::Sha1::build_digest(),
            #[cfg(feature = "sha2")]
            Self::Sha224 => sha2::Sha224::build_digest(),
            #[cfg(feature = "sha2")]
            Self::Sha256 => sha2::Sha256::build_digest(),
            #[cfg(feature = "sha2")]
            Self::Sha384 => sha2::Sha384::build_digest(),
            #[cfg(feature = "sha2")]
            Self::Sha512 => sha2::Sha512::build_digest(),
            #[cfg(feature = "sha2")]
            Self::Sha512_224 => sha2::Sha512_224::build_digest(),
            #[cfg(feature = "sha2")]
            Self::Sha512_256 => sha2::Sha512_256::build_digest(),
            #[cfg(feature = "sha3")]
            Self::Sha3_224 => sha3::Sha3_224::build_digest(),
            #[cfg(feature = "sha3")]
            Self::Sha3_256 => sha3::Sha3_256::build_digest(),
            #[cfg(feature = "sha3")]
            Self::Sha3_384 => sha3::Sha3_384::build_digest(),
            #[cfg(feature = "sha3")]
            Self::Sha3_512 => sha3::Sha3_512::build_digest(),
            #[cfg(feature = "shabal")]
            Self::Shabal192 => shabal::Shabal192::build_digest(),
            #[cfg(feature = "shabal")]
            Self::Shabal224 => shabal::Shabal224::build_digest(),
            #[cfg(feature = "shabal")]
            Self::Shabal256 => shabal::Shabal256::build_digest(),
            #[cfg(feature = "shabal")]
            Self::Shabal384 => shabal::Shabal384::build_digest(),
            #[cfg(feature = "shabal")]
            Self::Shabal512 => shabal::Shabal512::build_digest(),
            #[cfg(feature = "sm3")]
            Self::Sm3 => sm3::Sm3::build_digest(),
            #[cfg(feature = "streebog")]
            Self::Streebog256 => streebog::Streebog256::build_digest(),
            #[cfg(feature = "streebog")]
            Self::Streebog512 => streebog::Streebog512::build_digest(),
            #[cfg(feature = "tiger")]
            Self::Tiger => tiger::Tiger::build_digest(),
            #[cfg(feature = "tiger")]
            Self::Tiger2 => tiger::Tiger2::build_digest(),
            #[cfg(feature = "whirlpool")]
            Self::Whirlpool => whirlpool::Whirlpool::build_digest(),
        }
    }

    #[cfg(feature = "hmac")]
    fn new_mac_instance(&self, key: &[u8]) -> Box<dyn NamedDynDigest> {
        match *self {
            #[cfg(feature = "blake2")]
            Self::Blake2b => blake2::Blake2b512::build_mac_digest(key),
            #[cfg(feature = "blake2")]
            Self::Blake2s => blake2::Blake2s256::build_mac_digest(key),
            #[cfg(feature = "blake3")]
            Self::Blake3 => blake3::Hasher::build_mac_digest(key),
            #[cfg(feature = "fsb")]
            Self::Fsb160 => fsb::Fsb160::build_mac_digest(key),
            #[cfg(feature = "fsb")]
            Self::Fsb224 => fsb::Fsb224::build_mac_digest(key),
            #[cfg(feature = "fsb")]
            Self::Fsb256 => fsb::Fsb256::build_mac_digest(key),
            #[cfg(feature = "fsb")]
            Self::Fsb384 => fsb::Fsb384::build_mac_digest(key),
            #[cfg(feature = "fsb")]
            Self::Fsb512 => fsb::Fsb512::build_mac_digest(key),
            #[cfg(feature = "gost94")]
            Self::Gost94CryptoPro => gost94::Gost94CryptoPro::build_mac_digest(key),
            #[cfg(feature = "gost94")]
            Self::Gost94s2015 => gost94::Gost94s2015::build_mac_digest(key),
            #[cfg(feature = "groestl")]
            Self::Grøstl224 => groestl::Groestl224::build_mac_digest(key),
            #[cfg(feature = "groestl")]
            Self::Grøstl256 => groestl::Groestl256::build_mac_digest(key),
            #[cfg(feature = "groestl")]
            Self::Grøstl384 => groestl::Groestl384::build_mac_digest(key),
            #[cfg(feature = "groestl")]
            Self::Grøstl512 => groestl::Groestl512::build_mac_digest(key),
            #[cfg(feature = "md2")]
            Self::Md2 => md2::Md2::build_mac_digest(key),
            #[cfg(feature = "md4")]
            Self::Md4 => md4::Md4::build_mac_digest(key),
            #[cfg(feature = "md5")]
            Self::Md5 => md5::Md5::build_mac_digest(key),
            #[cfg(feature = "ripemd")]
            Self::Ripemd160 => ripemd::Ripemd160::build_mac_digest(key),
            #[cfg(feature = "ripemd")]
            Self::Ripemd256 => ripemd::Ripemd256::build_mac_digest(key),
            #[cfg(feature = "ripemd")]
            Self::Ripemd320 => ripemd::Ripemd320::build_mac_digest(key),
            #[cfg(feature = "sha1")]
            Self::Sha1 => sha1::Sha1::build_mac_digest(key),
            #[cfg(feature = "sha2")]
            Self::Sha224 => sha2::Sha224::build_mac_digest(key),
            #[cfg(feature = "sha2")]
            Self::Sha256 => sha2::Sha256::build_mac_digest(key),
            #[cfg(feature = "sha2")]
            Self::Sha384 => sha2::Sha384::build_mac_digest(key),
            #[cfg(feature = "sha2")]
            Self::Sha512 => sha2::Sha512::build_mac_digest(key),
            #[cfg(feature = "sha2")]
            Self::Sha512_224 => sha2::Sha512_224::build_mac_digest(key),
            #[cfg(feature = "sha2")]
            Self::Sha512_256 => sha2::Sha512_256::build_mac_digest(key),
            #[cfg(feature = "sha3")]
            Self::Sha3_224 => sha3::Sha3_224::build_mac_digest(key),
            #[cfg(feature = "sha3")]
            Self::Sha3_256 => sha3::Sha3_256::build_mac_digest(key),
            #[cfg(feature = "sha3")]
            Self::Sha3_384 => sha3::Sha3_384::build_mac_digest(key),
            #[cfg(feature = "sha3")]
            Self::Sha3_512 => sha3::Sha3_512::build_mac_digest(key),
            #[cfg(feature = "shabal")]
            Self::Shabal192 => shabal::Shabal192::build_mac_digest(key),
            #[cfg(feature = "shabal")]
            Self::Shabal224 => shabal::Shabal224::build_mac_digest(key),
            #[cfg(feature = "shabal")]
            Self::Shabal256 => shabal::Shabal256::build_mac_digest(key),
            #[cfg(feature = "shabal")]
            Self::Shabal384 => shabal::Shabal384::build_mac_digest(key),
            #[cfg(feature = "shabal")]
            Self::Shabal512 => shabal::Shabal512::build_mac_digest(key),
            #[cfg(feature = "sm3")]
            Self::Sm3 => sm3::Sm3::build_mac_digest(key),
            #[cfg(feature = "streebog")]
            Self::Streebog256 => streebog::Streebog256::build_mac_digest(key),
            #[cfg(feature = "streebog")]
            Self::Streebog512 => streebog::Streebog512::build_mac_digest(key),
            #[cfg(feature = "tiger")]
            Self::Tiger => tiger::Tiger::build_mac_digest(key),
            #[cfg(feature = "tiger")]
            Self::Tiger2 => tiger::Tiger2::build_mac_digest(key),
            #[cfg(feature = "whirlpool")]
            Self::Whirlpool => whirlpool::Whirlpool::build_mac_digest(key),
        }
    }
}

fn main() -> IoResult<()> {
    let options = ProcessedOptions::try_from(Options::parse())?;
    let mut hasher = options.new_hasher();
    if options.files.is_empty() {
        digest(hasher.as_mut(), stdin().lock())?;
        println!("{}", hex::encode(hasher.finalize()));
    } else {
        let name = hasher.algorithm_name();
        let mut failed = false;
        for ref arg in options.files {
            let lossy = arg.to_string_lossy();
            match digest_file(hasher.as_mut(), arg) {
                Ok(..) => print_hash(&name, &lossy, hasher.finalize_reset().into_vec(), options.reverse),
                Err(err) => {
                    failed = true;
                    eprintln!("{}: {}", lossy, err);
                    hasher.reset();
                }
            }
        }
        if failed {
            process::exit(1);
        }
    }
    Ok(())
}

fn digest_file<D: DynDigest + ?Sized, P: AsRef<Path>>(hasher: &mut D, path: P) -> IoResult<()> {
    digest(hasher, BufReader::new(File::open(path)?))
}

fn digest<D: DynDigest + ?Sized, R: BufRead>(hasher: &mut D, mut reader: R) -> IoResult<()> {
    loop {
        let buf = reader.fill_buf()?;
        match buf.len() {
            0 => return Ok(()),
            len => {
                hasher.update(buf);
                reader.consume(len);
            }
        }
    }
}

fn print_hash(algorithm_name: &str, file_name: &str, hash: Vec<u8>, reverse: bool) {
    let hex = hex::encode(hash);
    if reverse {
        println!("{hex} {file_name}");
    } else {
        println!("{algorithm_name} ({file_name}) = {hex}");
    }
}

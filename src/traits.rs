use digest::DynDigest;

pub trait Factory: 'static + DynDigest + StaticAlgorithmName + Default {
    fn build_digest() -> Box<dyn NamedDynDigest> {
        Box::new(Self::default())
    }

    #[cfg(feature = "hmac")]
    fn build_mac_digest(key: &[u8]) -> Box<dyn NamedDynDigest>;
}

pub trait NamedDynDigest: DynDigest + VirtualAlgorithmName {}

impl<D: DynDigest + VirtualAlgorithmName> NamedDynDigest for D {}

pub trait VirtualAlgorithmName {
    fn algorithm_name(&self) -> String;
}

impl<A: StaticAlgorithmName> VirtualAlgorithmName for A {
    fn algorithm_name(&self) -> String {
        A::algorithm_name()
    }
}

pub trait StaticAlgorithmName {
    fn algorithm_name() -> String;
}
